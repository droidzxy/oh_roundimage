# OhRoundImage

#### 组件介绍
-   本示例是OpenHarmony自定义组件RoundImage。

    用于定义一张图片为圆形或者圆角矩形。

#### 调用方法
 ```
<element name="roundimage" src="../roundimage/roundimage.hml"></element>
<div class="container">
    <roundimage path="/common/gitee.jpg" length="400" shape="1"></roundimage>
</div>
 ```

#### 参数介绍
| **参数**                      | **含义**                      |
| :-------------------------: | :------------------------------- |
| path                        | 设置图片路径             | 
| length                        | 设置图片宽度和高度，等宽高            |
| shape                        | 设置图片形状，0 - 圆形，1 - 圆角矩形    |


#### 效果演示

-   圆行图片
    
![效果演示](./screenshots/device/dog-round.png)  ![效果演示](./screenshots/device/gitee-round.png) 
   
-   圆角矩形图片

![效果演示](./screenshots/device/dog-rectangle.png)  ![效果演示](./screenshots/device/gitee-rectangle.png) 
